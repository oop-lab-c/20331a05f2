#include<iostream>
#include<iomanip>
using namespace std;
int main()
{
    string name="Hello World!";
    int a=1;
    int b=5,c=3;
    int d=b/c;
    cout<<name<<endl;
    cout<<a<<endl;
    cout<<"welcome to "<<endl<<"OOPS"<<ends<<"lab"<<endl;
    cout<<"Demonstration of setw"<<endl;
    cout<<"Hi"<<setw(10)<<"Welcome."<<endl;
    cout<<"Demonstration of setfill"<<endl;
    cout<<"Before setting the fill char:\n"<<setw(10)<<a<<endl;
    cout<<"After setting the fill char:\n"<<setw(10)<<setfill('*')<<a<<endl;
    cout<<"Demonstration of setprecision"<<endl;
    cout<<setprecision(1)<<d<<endl;
    cout<<setprecision(2)<<d<<endl;
    


    return 0;
}