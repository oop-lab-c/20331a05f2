
interface GPI {

	
	default void show()
	{
		System.out.println("Default GPI");
	}
}

interface PI1 extends GPI {
}

interface PI2 extends GPI {
}


class MultipleInheriJava implements PI1, PI2 {


	public static void main(String args[])
	{

        MultipleInheriJava d = new MultipleInheriJava();
		d.show();
	}
}
