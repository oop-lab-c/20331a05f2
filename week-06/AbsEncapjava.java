class AccessSpecifierDemo{
    private
    int PriVar;
    protected
    int ProVar;
    public
    int PubVar;
    
    void setVar(int priVal,int pubVal,int proVal){
        PriVar=priVal;
        ProVar=proVal;
        PubVar=pubVal;


    }
    void getVar(){
        System.out.println("The Private Variable Value is:"+PriVar);
        System.out.println("The Public Variable Value is:"+PubVar);
        System.out.println("The Protected Variable Value is:"+ProVar);
        
    }
}
public class AbsEncapjava{

    public static void main(String[] args){
        AccessSpecifierDemo obj = new AccessSpecifierDemo();
        obj.setVar(5, 10, 15);
        obj.getVar();

    }
}
