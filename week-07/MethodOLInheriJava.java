import java.util.Scanner;
class Number{
    int a,b,c;
    Number(){
        System.out.println("this is base class constructor");
    }
    void addfunction(int a,int b){
        this.a=a;
        this.b=b;
        c=a+b;
        System.out.println(c);


    }
}
class StringClass extends Number{
    String a,b,c;
    StringClass(){
        System.out.println("this is derived class constructor");
    }
    void addfunction(String a, String b){
        this.a=a;
        this.b=b;
        c=a+b;
        System.out.println(c);
    }
    

}


public class MethodOLInheriJava {
     public static void main(String[] args) {
        int a,b;
        String c,d;
        Scanner sc=new Scanner(System.in);
       
        System.out.println("enter the  string:");
        c=sc.nextLine();
        System.out.println("enter the string:");
        d=sc.nextLine();
        System.out.println("enter the value of a:");
        a=sc.nextInt();
        System.out.println("enter the value of b:");
        b=sc.nextInt();

        StringClass obj=new StringClass();
        obj.addfunction(a, b);
        
        
        StringClass obj1=new StringClass();
        obj1.addfunction(c, d);


        


    }
    
}
