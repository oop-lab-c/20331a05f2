
class Base{
    int a,b,c;
    Base(){
        System.out.println("This is Base Class");
    }
    void addFunction(int a,int b){
        System.out.println("This is base class function");
         this.a=a;
         this.b=b;
         c=a+b;
         System.out.println(c);
    }
}
class Derived extends Base{
    int a,b,c;
    Derived(){
    System.out.println("This is derived class");
    }
    void addFunction(int a,int b){
        System.out.println("This is derived class function");
        this.a=a;
        this.b=b;
        c=a+b;
        System.out.println(c);
   }
    
}


public class MethodORJava {
    public static void main(String[] args) {
        Derived obj=new Derived();
        obj.addFunction(5, 10);

    }
}
