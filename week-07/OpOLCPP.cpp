#include<iostream>
using namespace std;
class complex{
    public:
    float x;
    float y;
    
    complex(){};
    complex(float real,float img){
        x=real;
        y=img;
    }
    complex operator +(complex);
    
    
    void display(void);
};
complex complex::operator +(complex c){
    complex temp;
    temp.x=x+c.x;
    temp.y=y+c.y;
    return temp;

}
void complex::display(void){
    cout<<x<<"+"<<y<<"j\n";
}
int main(){
    complex c1,c2,c3;
    c1=complex(1,2);
    c2=complex(5,6);
    c3=c1.operator +(c2); //same as c3=c1+c2
    c1.display();
    c2.display();
    cout<<"After Adding Two complex Numbers:";
    c3.display();

}

