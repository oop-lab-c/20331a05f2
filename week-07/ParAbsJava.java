
abstract class Base {
    public
    Base(){
        System.out.println("Base Constructor called");
    }
	abstract void fun();
    void display(){
        System.out.println("This is not a abstract function");
    }

}
class Derived extends Base {
	void fun()
	{
		System.out.println("Derived fun() called");
	}
}
class  ParAbsJava{
public static void main(String args[])
	{

		
		Derived b = new Derived();
		b.fun();
        b.display();

	}
}










