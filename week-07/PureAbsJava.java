interface Base{
    final int a=10,b=20;

    
    void show();
    void add();

}
class Derived implements Base{
    Derived(){
        System.out.println("This is Derived Class Constructor.Interface classes Cannot Have constructors");

    }
    public void show(){
        System.out.println("the values of a and b are:"+a+" "+b);
    }
     public void add(){
        System.out.println("The sum of two numbers is:"+(a+b));


    }
}

public class PureAbsJava {
    public static void main(String[] args) {
        Derived Obj=new Derived();
        Obj.show();
        Obj.add();
    }
    
}
