#include<iostream>
using namespace std;
class parabstraction{
    public:
    int x,y;
    parabstraction(int x, int y){
        this->x=x;
        this->y=y;
    }
    virtual int add()=0;
    void display(int tot){
        cout<<"the total is:"<<tot<<endl;
            }
};
class derived:public parabstraction{
    public:
    
    derived(int x, int y):parabstraction(x,y){};
    int add(){
        return x+y;
        
    }

};
int main(){
    int t;
    derived obj(5,10);
    t=obj.add();
    obj.display(t);
    return 0;

}