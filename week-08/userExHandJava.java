//Java Program to demonstrate the userdefined exceptions
import java.util.*;
public class userExHandJava {
    public static void main(String[] args)
    {
        try        //try block
        {
            throw new MyException(5); //throw keyword is used in creating userdefined exceptions
        }
        catch(Exception e)  //catch block
        {
            System.out.println(e);
        }
        finally    //finally block
        {
            System.out.println("User defined exceptions are handled...");
        }
    }
}
class MyException extends Exception
{
    int a;
    MyException(int b)
    {
     a=b;
     System.out.println(a+b);
    }
}